# GalaxyGen

This is the data pipeline repository for preparing astronometric information to feed into any number of game use-cases, but with a primary focus on preparation for a Galaxy map plugin in Godot.

## Getting started

It's pretty simple - you just need python 3.6 or 3.7

* Install from requirements.txt
* Run ./run_data.py

Right now this repo will just spit out a handful of 

## Current Features

- [x] Pulling data from the Gaia Nearby Stars Catalog (DR3 early release)
- [x] Pulling object metadata data from SIMBAD for parallax above 10 mas (100 parsec) - mostly for altnerate star names
- [x] Combining the Gaia and SIMBAD catalog data and converting to galactic cartesian

## Upcoming Features

- [ ] Figuring out spectral type for the 150,000+ stars in Gaia that do not have estimates 
- [ ] Parent Child Modelling - Multi-star systems (binaries++) 
- [ ] Data Pipeline Target Abstraction ( generating files, writing to various DB types based on your preference )

## Longer Term Aspirations

- [ ] Pulls from Multiple Star catalogs of somekind - IE binary star orbit / separation data
- [ ] Pulls from Exoplanet sources - Caltech, NASA, Exoplanets.org, TESS (directly)
- [ ] Any information / likelihood of planetanetary debris disks
- [ ] Some method that is less pseudo-science than Titius-Bode for inferring smaller undetected planets in known systems or generating for empty systems

