#!/usr/bin/python

from ast import Return
import requests
import re
import urllib
import pyvo as vo
from zipfile import ZipFile
import astropy.units as u
# from astropy.coordinates import SkyCoord
import astropy.coordinates as coord
from astroquery.gaia import Gaia
from io import StringIO
from io import BytesIO
from os.path import exists
import pandas as pd
import numpy as np
from tqdm import tqdm
from pandasql import sqldf

def convertMag(plx, bp_rp):
    return (-1.0 * (5.0 * math.log(0.1/plx) ) - bp_rp)

def temp_r(x, x0, x1, y0, y1):
    rx0 = x0
    rx1 = x1
    
    if x0 > x1:
        rx0 = x1
        rx1 = x0
    
    if x < rx0:
        return y0
    elif x > rx1:
        return y1
    else:
        return (y0 + (y1 - y0)*(x -rx0)/(rx1 - rx0))

def zipFrame(url, savepath, savename):
    print('Retrieving files from: '+url)
    r = urllib.request.urlopen(url).read()

    r = requests.get(url, stream = True)
  
    with open(savepath+savename+'.dat.gz',"wb") as pdf:
        for chunk in r.iter_content(chunk_size=1024):
            # writing one chunk at a time to pdf file
            if chunk:
                pdf.write(chunk)
    print('file saved')

class StarData:
    def __init__(self, override):
        self.override = override
        self.data_path = 'data/file_get/'
        self.output_path = 'data/output/'
        # Flat/Binary, Database OR OpenDB
        self.output_type = 'flat'
        # Secrets
        self.StarSystems = None
        self.AllObjects = None
        self.generate_planets = False

        # Grab base data from Gaia
        # TODO - this is just GNSC for now - paging for ALL data later
        self.data_Gaia = self.get_Gaia()
        
        # Grab data from Simbad
        self.data_Simbad_Basic = self.get_simbad_basic()
        
        # Join it all up!
        self.join_Gaia_SIMBAD()

    def fetch_gaia(self):
        # Fetch data is the function that actually grabs and prepares Gaia data
        print('Fetching the Gaia data')

        # Full catalog available here - but VERY large
        # https://www.astro.rug.nl/~gaia/
        # TODO - long term - host DB that streams star data in
        # For now - this is 100 Parsec Limit
        # https://cdsarc.cds.unistra.fr/viz-bin/cat/J/A+A/649/A6#/browse
        
        # Intead we'll use astroquery to pull pre-release DR3
        # Then enrich with the more scrutinous "Gaia Catalog og Nearby Star GCNS"
        query = """
            SELECT 
                 dr3.solution_id
            ,    dr3.designation
            ,    dr3.source_id
            ,    dr3.ref_epoch
            ,    dr3.ra
            ,    gcn.ra AS gcn_ra
            ,    dr3.ra_error
            ,    dr3.dec
            ,    dr3.dec AS dec_gcn
            ,    dr3.parallax
            ,    dr3.parallax_error
            ,    dr3.l
            ,    dr3.b
            ,    dr3.ecl_lon
            ,    dr3.ecl_lat
            ,    dr3.phot_g_mean_mag
            ,    dr3.phot_bp_mean_mag
            ,    dr3.phot_rp_mean_mag
            ,    dr3.bp_rp
            ,    dr3.bp_g
            ,    dr3.g_rp
            ,    gcn.dist_50
            ,    gcn.xcoord_50
            ,    gcn.ycoord_50
            ,    gcn.zcoord_50
            ,    gcn.uvel_50
            ,    gcn.vvel_50
            ,    gcn.wvel_50
            ,    gcn.name_gunn
            ,    gcn.name_2mass
            ,    gcn.name_wise
        FROM external.gaiaedr3_gcns_main_1 gcn
        LEFT JOIN  gaiaedr3.gaia_source dr3
            ON gcn.source_id = dr3.source_id
        """
        # Color to SpType
        # http://www.pas.rochester.edu/~emamajek/EEM_dwarf_UBVIJHK_colors_Teff.txt

        # Cross-Reference GCNS

        job = Gaia.launch_job_async(query)
        r = job.get_results()
        df_job = r.to_pandas()
        df_job.to_csv(self.data_path+'gaia_nsc_100pc.csv')


    def get_Gaia(self):
        # GET Gaia is the function to check if data exists and then pull,
        # it won't run the pull unless there is no file or OVERRIDE = True
        # https://cdsarc.cds.unistra.fr/viz-bin/cat/J/A+A/649/A6#/browse

        # Todo - check for file
        if self.override == True or not exists(self.data_path+'gaia_nsc_100pc.csv'):
            # GET THE DATA
            print('Fetching the data')
            self.fetch_gaia()
            # Return file 
        else:
            # Check for the file
            print('No need to fetch the data')
            # return
        
        df_gaia = pd.read_csv(self.data_path+'gaia_nsc_100pc.csv')
        return df_gaia


    def fetch_simbad(self, filename, query):
        print('Fetching the Simbad data')
        # https://astroquery.readthedocs.io/en/latest/simbad/simbad.html
        # https://pyvo.readthedocs.io/en/latest/#registry-search 
        
        simbad_service = vo.dal.TAPService("http://simbad.u-strasbg.fr/simbad/sim-tap")
        
        if self.override == True or not exists(self.data_path+filename):
            df_simbad = pd.DataFrame(simbad_service.search(query))
            df_simbad.to_csv(self.data_path+filename, sep ='\t')
        else:
            # Check for the file
            print('No need to fetch the data')
            # return
        
        df_simbad_basic = pd.read_csv(self.data_path+filename, sep ='\t', low_memory=False)
        #print(df_simbad_basic)
        return df_simbad_basic


    def get_simbad_basic(self):
        file_simbad_basic_18 = 'simbad_basic_p18+.tsv'
        
        q_base = """SELECT
                        B.main_id	,
                        B.oid	,
                        I.ids ,
                        B.otype	,
                        B.otype_txt	,
                        B.ra	,
                        B.dec	,
                        B.plx_value	,
                        B.pmra	,
                        B.pmdec	,
                        B.galdim_majaxis ,
                        B.galdim_minaxis ,
                        B.galdim_angle	,
                        B.sp_type	,
                        F.B AS col_B,
                        F.V AS col_V,
                        F.g_,
                        F.r_,
                        AVG(T.teff) AS temp_eff,
                        AVG(T.fe_h) AS fe_h,
                        H.parent AS simbad_parent_oid ,
                        H.membership AS est_parent_percent,
                        B.update_date		
                    FROM BASIC B
                    LEFT JOIN IDS I
                    ON B.oid = I.oidref
                    LEFT JOIN H_LINK H
                    ON B.oid = H.child
                    LEFT JOIN ALLFLUXES F
                    ON B.oid = F.oidref
                    LEFT JOIN MESFE_H T
                    ON B.oid = T.oidref
                    """
        q_ob = """GROUP BY B.main_id	,
                        B.oid	,
                        I.ids ,
                        B.otype	,
                        B.otype_txt	,
                        B.ra	,
                        B.dec	,
                        B.plx_value	,
                        B.pmra	,
                        B.pmdec	,
                        B.galdim_majaxis ,
                        B.galdim_minaxis ,
                        B.galdim_angle	,
                        B.sp_type	,
                        F.B,
                        F.V,
                        F.g_,
                        F.r_,
                        H.parent,
                        H.membership,
                        B.update_date	
                    ORDER BY plx_value DESC"""
        
        q_basic_18 = q_base + """ 
                    WHERE plx_value IS NOT NULL
                    AND plx_value >= 18
                    """ + q_ob

        df_18 = self.fetch_simbad(file_simbad_basic_18, q_basic_18)
        
        # TODO - wrap the ranged simbad queries in a text generator function
        file_simbad_basic_13_18 = 'simbad_basic_p13_18.tsv'        
        q_basic_13_18 = q_base + """ 
                    WHERE plx_value IS NOT NULL
                    AND plx_value < 18
                    AND plx_value >= 13 
                                """ + q_ob
        df_1318 = self.fetch_simbad(file_simbad_basic_13_18, q_basic_13_18)

        file_simbad_basic_105_13 = 'simbad_basic_p13_18.tsv'        
        q_basic_105_13 = q_base + """
                        WHERE plx_value IS NOT NULL
                            AND plx_value < 13
                            AND plx_value >= 10.5 
                 """ + q_ob
        df_10513 = self.fetch_simbad(file_simbad_basic_105_13, q_basic_105_13)
        
        file_simbad_basic_95_105 = 'simbad_basic_p13_18.tsv'        
        q_basic_95_105 = q_base + """
                        WHERE plx_value IS NOT NULL
                            AND plx_value < 10.5
                            AND plx_value >= 9 
                 """ + q_ob
        df_95105 = self.fetch_simbad(file_simbad_basic_95_105, q_basic_95_105)

        # Stack the frames
        # df_18, df_1318, df_10513, df_95105
        df_simbad = pd.concat([df_18, df_1318, df_10513, df_95105], axis=0, ignore_index=True)
        print(df_simbad.head(5))
        df_simbad.to_csv(self.data_path+'simbad_aggregated.tsv', sep ='\t')
        return df_simbad

    def join_Gaia_SIMBAD(self):
        # This is where 
        print('Joining Gaia Near Stars Data to SIMBAD (9.5 mas plx +)')
        # self.data_Gaia
        # self.data_Simbad_Basic

        df_core = self.data_Gaia.copy()
        df_simbad = self.data_Simbad_Basic.reindex().copy()
        # df_core[ df_core.designation.apply( lambda title: self.data_Simbad_Basic.IDS.str.contains(title) ).any(1) ]
        # df1[ df1['movie'].apply(lambda title: df2['FILM'].str.contains(title)).any(1) ]
        # df_combined = df_simbad.assign(designation=df_simbad['ids'].str.split('|').str[-1]).merge(df_core, on='designation', how='outer')
        # idx = df_core['designation'].apply(lambda x: df_simbad.index[df_simbad['ids'].str.contains(x)][0])
        # df_combined = df_core.loc[idx].join(df_simbad)
        
        # USE REGEX
        # https://stackoverflow.com/questions/46350705/creating-new-column-in-pandas-dataframe-using-regex/46351084
        # (?<=Gaia EDR3 )(.*?)(?=\|)
        search = []    
        for values in df_simbad['ids']:
                try:
                    gr3 = 'Gaia EDR3 ' + re.search(r'(?<=Gaia EDR3 )(.*?)(?=\|)', values+'|').group()
                except:
                    gr3 = 'None'
                # print('gr3: '+gr3)
                search.append(gr3)

        # Set a column for the identified regex extracted value
        df_simbad['gaia_3'] = search
        
        ### MERGE
        # Perform the dataframe merge - as an outer join, folding Gaia and SIMBAD data together
        df_combined = df_core.merge(df_simbad, left_on='designation', right_on='gaia_3', how='outer')
        
        # Run Galactocentric Cartesian Coordinate Conversions for SIMBAD data
        simbad_ra = df_combined['ra_y'].values * u.degree
        simbad_dec = df_combined['dec_y'].values * u.degree
        df_combined['simbad_dist'] = float(1.0)/(df_combined['plx_value']/float(1000.0))
        simbad_dist = df_combined['simbad_dist'].values * u.pc
        c = coord.ICRS(simbad_ra, simbad_dec, simbad_dist)
        gal_xyz = c.transform_to(coord.Galactocentric)
        df_combined['simbad_x'] = pd.Series(gal_xyz.x.value.tolist(), index=df_combined.index)
        df_combined['simbad_y'] = pd.Series(gal_xyz.y.value.tolist(), index=df_combined.index)
        df_combined['simbad_z'] = pd.Series(gal_xyz.z.value.tolist(), index=df_combined.index)

        # Now do same converstion for Gaia data
        simbad_ra = df_combined['ra_x'].values * u.degree
        simbad_dec = df_combined['dec_x'].values * u.degree
        df_combined['gaia_dist'] = float(1.0)/(df_combined['parallax']/float(1000.0))
        simbad_dist = df_combined['gaia_dist'].values * u.pc
        # TODO - I think Gaia Epoch is 2016 not 2000
        c = coord.ICRS(simbad_ra, simbad_dec, simbad_dist)
        gal_xyz = c.transform_to(coord.Galactocentric)
        df_combined['gaia_x'] = pd.Series(gal_xyz.x.value.tolist(), index=df_combined.index)
        df_combined['gaia_y'] = pd.Series(gal_xyz.y.value.tolist(), index=df_combined.index)
        df_combined['gaia_z'] = pd.Series(gal_xyz.z.value.tolist(), index=df_combined.index)
            
        q_sys = """SELECT 
                       coalesce(replace(main_id, 'NAME ', ''), designation) as sys_proper_name
                    ,  coalesce(ra_x, ra_y) AS sys_ra
                    ,  coalesce(dec_x, dec_y) AS sys_dec
                    ,  coalesce(parallax, plx_value) AS sys_plx
                    ,  coalesce(gaia_x, simbad_x) AS sys_gal_x
                    ,  coalesce(gaia_y, simbad_y) AS sys_gal_y
                    ,  coalesce(gaia_z, simbad_z) AS sys_gal_z
                    ,  oid AS sys_simbad_oid
                    ,  otype_txt AS sys_simbad_type
                    ,  ids AS sys_simbad_alt_ids
                    ,  update_date AS sys_update_date -- #TODO: Get Gaia update dates and coallesce
                    -- ,  simbad_gal_b
                    -- ,  l as l_gal_long
                    -- ,  l as b_gal_lat
                FROM df_combined
                WHERE simbad_parent_oid < 0.0
                OR otype_txt = '**'
                ORDER BY coalesce(parallax, plx_value) DESC
            """
        # print('global vars: ', globals())
        pysqldf = lambda q: sqldf(q, {'df_combined':df_combined})
        df_sys = pysqldf(q_sys)

        print(df_sys.head(5))
        df_sys.to_csv(self.output_path+'gg_systems.tsv', sep ='\t')


        ### STARS - now for stars, which are children to systems
        # CXP ≡ GBP−GRP   
        #   phot_g_mean_mag
        #   phot_bp_mean_mag
        #   phot_rp_mean_mag
        #   bp_rp
        #   Convert color mag to absolute
        #   M = -1 * (5 log (d/10)) - m
        # df_combined['BPRP'] = -1.0 * (5.0 * math.log(0.1/df_combined['plx_value']) ) - df_combined['bp_rp']
        # df_combined['BPRP']= df_combined.apply(lambda row : convertMag(row['plx_value'],df_combined['bp_rp'])
        #                                        , axis = 1)
        # df_combined['BPRP'] = -1.0 * (5.0 * np.log10(0.1/df_combined['plx_value']) ) - df_combined['bp_rp']
        df_combined['BPRP'] = -1.0 * (5.0 * np.log10(0.1/df_combined['plx_value']) ) - df_combined['bp_rp']

        # log(Teff) = 3.999−0.654(C_XP)+0.709(C_XP)^2−0.316(C_XP)^3
        df_combined['temp_gaia_g'] = 10.0 ** ( 3.999 - 0.654*(df_combined['BPRP']) + \
                                             0.709 * (df_combined['BPRP'])**2.0 - \
                                             0.316 * (df_combined['BPRP'])**3.0 )
        
        # temp_r(df_combined['BPRP'], 1.5, 15.0, 3521.6, 3000.0)
        # df_combined['temp_gaia_r'] = temp_r(df_combined['BPRP'].values, 1.5, 15.0, 3521.6, 3000.0)
        # df_combined['temp_gaia_r'] = df_combined.apply(lambda row: temp_r(row.BPRP, 1.5, 15.0, 3521.6, 3000.0))
        # df_combined['temp_gaia_r'] = df_combined.Set.map( lambda x: temp_r(x['BPRP'], 1.5, 15.0, 3521.6, 3000.0))
        df_combined['temp_gaia_r'] = df_combined['BPRP'].apply(lambda row: temp_r(row, 1.5, 15.0, 3521.6, 3000.0))
        # df_combined['temp_gaia_r'] = 
        df_combined[['star_1_type', 'star_2_type']] = df_combined['otype_txt'].str.split('+', expand=True)

        q_star = """SELECT DISTINCT
                       coalesce(replace(main_id, 'NAME ', ''), designation) as sys_proper_name
                    ,  coalesce(ra_x, ra_y) AS sys_ra
                    ,  coalesce(dec_x, dec_y) AS sys_dec
                    ,  coalesce(parallax, plx_value) AS sys_plx
                    ,  coalesce(gaia_x, simbad_x) AS sys_gal_x
                    ,  coalesce(gaia_y, simbad_y) AS sys_gal_y
                    ,  coalesce(gaia_z, simbad_z) AS sys_gal_z
                    ,  oid AS sys_simbad_oid
                    ,  star_1_type AS star1_simbad_type
                    ,  star_2_type AS star2_simbad_type
                    ,  ids AS sys_simbad_alt_ids
                    ,  bp_rp
                    ,  BPRP
                    ,  sp_type AS spec_type
                    ,  temp_eff AS simbad_temp
                    ,  temp_gaia_g
                    ,  temp_gaia_r
                    ,  CASE WHEN BPRP <= 1.5 THEN temp_gaia_g ELSE temp_gaia_r
                        END AS temp_gaia
                    ,  update_date AS sys_update_date -- #TODO: Get Gaia update dates and coallesce
                    -- ,  simbad_gal_b
                    -- ,  l as l_gal_long
                    -- ,  l as b_gal_lat
                FROM df_combined
                WHERE simbad_parent_oid < 0.0
                AND otype_txt NOT IN ('**', 'Pl', 'Pl?')
                ORDER BY coalesce(parallax, plx_value) DESC
                -- UNION WHERE otype = ** but split
            """
        
        df_star = pysqldf(q_star)
        print('DF STAR')
        print(df_star.head(5))
        df_star.to_csv(self.output_path+'gg_star.tsv', sep ='\t')



    def get_Exoplanets():
        print('Fetching the Simbad data')
    
    def load_MultipleStarCatalog():
        print('Fetching the Simbad data')

    def AggregateSystems():
        print('Aligning Gaia to SIMBAD')

        # Export 2 objects, systems and key_objects
        # Key_Objects should include stars, planemos and various Nebula

    def AggregatePlanets():
            print('Aligning Gaia to SIMBAD')
            
            # Planet Data Against Systems Data

    # Generate using Titius Bode Law
    # def generatePlanets(fill_known, ly_radius):    
        # Boolean generate_planets
        # REF 2nd python file - import class to generate solar system
        # Also have argument for generating stars for EMPTY systems -
        #  and IF so, then for how many light years out?
        #

def init_data(override):
    print('Override: '+str(override))

    # Init the Model
    SD = StarData(override)

    # Stucture out Star Systems vs Stars (IE Binary Pairs)


if __name__ == "__main__":
    print('Kicking off data job')
    init_data(override = False)
